import Header from './components/Header';
import {
  QueryClient,
  QueryClientProvider,
} from 'react-query';
import { NavigationContent } from './components/Navigation';
import { GlobalStateProvider } from './context/GlobalContext';
import { Helmet } from "react-helmet";

function App() {

  const queryClient = new QueryClient()
  return (
    <GlobalStateProvider>
      <QueryClientProvider client={queryClient}>
        <div className="">
          <Helmet>
            <meta charSet="utf-8" />
            <meta name="description" content="Mercado Libre Argentina - Homepage" />
            <meta name="theme-color" content="#faf159" />
            <title>Mercado Libre Argentina - Envíos Gratis en el día </title>
          </Helmet>
          <Header />
          <NavigationContent />
        </div>
      </QueryClientProvider>
    </GlobalStateProvider>
  );
}

export default App;
