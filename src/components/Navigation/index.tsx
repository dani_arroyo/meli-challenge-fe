import {
    Routes,
    Route
} from "react-router-dom";
import ResultDetail from "../ResultDetail";
import ResultSearch from "../ResultSearch";

export function NavigationContent() {

    return (
        <Routes>
            <Route path="/" element={<div />} />
            <Route path="items" element={<ResultSearch />} />
            <Route path="items/:id" element={<ResultDetail />} />
        </Routes>
    );
}