import React from 'react';
import styled from 'styled-components';
import { useSearchParams } from 'react-router-dom';
import getItemsSearch from '../../services/getSearchItems';
import Spinner from '../ui/Spinner'
import ItemSearch from './ItemSearch';
import { Helmet } from "react-helmet";

const Index: React.FC = () => {

    let [searchParams] = useSearchParams();
    let searchQuery = searchParams.get("search");
    const [items, setItems] = React.useState<Array<any> | null>(null);
    const [categories, setCategories] = React.useState<Array<any>>([]);
    const [isLoading, setIsLoading] = React.useState<boolean>(false);

    React.useEffect(() => {
        if (searchQuery) {
            setIsLoading(true)
            getItemsSearch(searchQuery).then(response => {
                if (!response.isError) {
                    setItems(response.data.items)
                    setCategories(response.data.categories)
                    setIsLoading(false)
                }
            })
        }
    }, [searchQuery])

    return (
        <Wrapper>
            <Helmet>
                <meta charSet="utf-8" />
                <meta name="description" content="Mercado Libre Argentina- Resultados de búsqueda." />
                <meta name="theme-color" content="#faf159" />
                <title>{`${searchQuery ? searchQuery + ' - ' : ''} Mercado Libre Argentina`} </title>
            </Helmet>

            {categories.length > 0 && <Breadcrumb>
                {categories.map(item => {
                    return ` ${item} >`
                })}
            </Breadcrumb>}
            {isLoading && <Spinner />}
            {items && items.length > 0 ? items.map((item: any) => {
                return (
                    <ItemSearch
                        key={item.id}
                        config={item}
                    />
                )
            }) : items && items.length === 0 ? (
                <p> No results</p>
            ) : null}
        </Wrapper>
    )
};

export default Index;

const Wrapper = styled.div`
    display:grid;
    width:100vw;
    box-sizing:border-box;
    @media (min-width: 768px) {
        padding:0 9em;
    }
`
const Breadcrumb = styled.p`
font-family:'Proxima Nova';
font-size:14px;
color:#999999;
cursor:pointer;
padding: 0 16px;
@media (min-width: 768px) {
    padding:0
}
`